package messfairy.controller;

import java.util.List;
import messfairy.dao.UsersDao;
import messfairy.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/users"})
public class UsersRestController
{
    @Autowired
    private UsersDao userDao;
    private List<User> users;

    @RequestMapping(value={""}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
    public List<User> list()
    {
        List<User> users = this.userDao.queryAll();
        return users;
    }

    @RequestMapping(value={"/{id}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
    public User getUser(@PathVariable("id") int id)
    {
        User user = this.userDao.byId(id);
        return user;
    }
}
